from django.shortcuts import render, redirect
from tasks.forms import CreateForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required

# Create your views here.


def create_task(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateForm()
    context = {"form": form}
    return render(request, "tasks/create.html", context)


@login_required
def my_task_list(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "my_tasks": tasks,
    }
    return render(request, "tasks/my_list.html", context)
