from django.urls import path
from accounts.views import login, user_logout, signup

urlpatterns = [
    path("signup/", signup, name="signup"),
    path("login/", login, name="login"),
    path("logout/", user_logout, name="logout"),
]
